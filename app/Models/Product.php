<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    public function commands()
    {
        return $this->belongsToMany(Command::class,'commands_products', 'productid', 'commandid');
    }

    public function customers()
    {
        return $this->belongsToMany(Customer::class,'inventaris', 'productid', 'costumerid')->withPivot("quantity");
    }

    public function categories()
    {
        return $this->belongsTo(Categorie::class,'categorieid');
    }
    use HasFactory;
}
