<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    public function customer()
    {
        return $this->hasMany(Product::class,"categorieid");
    }

    use HasFactory;
}
