<?php

namespace App\Models;

use App\Http\Controllers\Products;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function commands(){
        return $this->hasMany(Command::class, 'commandid');
    }

    public function products(){
        return $this->belongsToMany(Product::class, 'inventaris', 'customerid','productid')->withPivot("quantity");
    }

    use HasFactory;
}
