<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use Illuminate\Http\Request;

class CategorieController extends Controller
{
    public function showCategories()
    {
        $Categorie = Categorie::all();
        return view('categorie_list')->with('categories',$Categorie);
    }

    public function deleteCategorie(Categorie $categorie)
    {
        $categorie->delete();
        return redirect('/categories');
    }

    public function addCategorie(Request $request)
    {

        $validateData = $request->validate([
            'name'=>'required|max:255',
            'percent'=>'required'
        ]);
        $categorie = new Categorie();
        $categorie->name = $request->input('name');
        $categorie->percent = $request->input('percent');
        $categorie->save();
        return redirect('/categories');
    }

    public function showForm(){
        return view('add_categorie');
    }
}
