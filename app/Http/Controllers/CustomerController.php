<?php

namespace App\Http\Controllers;
use \App\Models\Customer;
use App\Models\Product;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
        $Customer = Customer::all();
        return view('customer-list')->with('customers',$Customer);
    }

    public function deleteCustomer(Customer $customer)
    {
        $customer->delete();
        return redirect('/customers');
    }

    public function addCustomer(Request $request)
    {

        $validateData = $request->validate([
            'name'=>'required|max:255',
            'address'=>'required',
        ]);
        $customer = new Customer();
        $customer->name = $request->input('name');
        $customer->address = $request->input('address');
        $customer->save();
        return redirect('/customers');
    }

    public function showForm(){
        return view('add_customers');
    }
}
