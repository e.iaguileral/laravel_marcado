<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class InventariController extends Controller
{

    public function showInventori(Customer $customer)
    {
        return view('inventari_list')->with('customer',$customer);
    }
}
