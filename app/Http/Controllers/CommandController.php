<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Command;
use App\Models\Commands_product;
use App\Models\Customer;
use App\Models\Inventari;
use App\Models\Product;
use Illuminate\Http\Request;
use Carbon\Carbon;
class CommandController extends Controller
{
    public function formCommand()
    {
        $Customer = Customer::all();
        $Products = Product::all();
        $fallo = false;
        return view('form_command_client')->with('products',$Products)->with('customers',$Customer)->with('fallo',$fallo);
    }

    public function allCommand()
    {
        $Command = Command::with('customer','products')->get();
        return view('commands')->with('commands',$Command);
    }

    public function addCommand(Request $request)
    {
        $customer = Customer::find($request->input('customer'));
        $product = Product::find($request->input('product'));
        $categorie = Categorie::find($product->categorieid);
        $inventario = Inventari::where('customerid',$customer->id)->where('productid',$product->id)->first();
        if($request->input('quantity') == null || $product->quantity < $request->input('quantity') || 0 >= $request->input('quantity')){
            $fallo = true;
            $Customer = Customer::all();
            $Products = Product::all();

            return view('form_command_client')->with('products',$Products)->with('customers',$Customer)->with('fallo',$fallo);
        }else{
            $command = new Command();
            $command->customerid = $request->input('customer');
            $command->data = Carbon::now()->toDate();
            $command->total_price = $product->price*$request->input('quantity');
            $command->total_price_IVA = $product->price*$request->input('quantity')+($product->price*$request->input('quantity')*$categorie->percent/100);
            $command->save();

            $product->quantity = $product->quantity-$request->input('quantity');
            $product->save();

            $cp = new Commands_product();
            $cp->commandid = $command->id;
            $cp->productid = $product->id;
            $cp->price = $product->price;
            $cp->iva = $categorie->percent;
            $cp->quantity = $request->input('quantity');
            $cp->save();
            if($inventario){
                $inventario->quantity = $inventario->quantity+$request->input('quantity');
                $inventario->update();
            }else{
                $inv = new Inventari();
                $inv->customerid = $customer->id;
                $inv->productid = $product->id;
                $inv->quantity = $request->input('quantity');
                $inv->save();
            }

            return redirect('/formCommandCustomer');
        }

    }
}
