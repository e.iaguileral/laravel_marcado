<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Product;
use Illuminate\Http\Request;
use function Laravel\Prompts\table;

class Products extends Controller
{
    public function index()
    {
        $Products = Product::with('categories')->get();
        return view('products-list')->with('products',$Products);
    }

    public function deleteProduct(Product $product)
    {
        $product->delete();
        return redirect('/products');
    }

    public function addProduct(Request $request)
    {

        $validateData = $request->validate([
           'name'=>'required|max:255',
           'price'=>'required',
           'quantity'=>'required',
           'description'=>'required',
        ]);
        $product = new Product();
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->quantity = $request->input('quantity');
        $product->description = $request->input('description');
        $product->categorieid = $request->input('categorie');
        $product->save();
        return redirect('/products');
    }

    public function showForm(){
        $Categorie = Categorie::all();
        return view('add_products')->with('categories',$Categorie);
    }
}
