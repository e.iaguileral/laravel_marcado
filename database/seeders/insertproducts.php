<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class insertproducts extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('products')->insert([
           ['name'=>'Poción','price'=>20,'quantity'=>5,'description'=>'description','categorieid'=>1],
            ['name'=>'Keysword','price'=>120,'quantity'=>1,'description'=>'description','categorieid'=>2],
            ['name'=>'Flame Armor','price'=>75,'quantity'=>1,'description'=>'description','categorieid'=>3]
        ]);
    }
}
