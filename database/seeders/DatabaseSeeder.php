<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('categories')->insert([
            ['name'=>'Categoria 21%','percent'=>21],
            ['name'=>'Categoria 15%','percent'=>15],
            ['name'=>'Categoria 50%','percent'=>50],
        ]);

        DB::table('products')->insert([
            ['name'=>'Poción','price'=>20,'quantity'=>20,'description'=>'description','categorieid'=>1],
            ['name'=>'Espada','price'=>120,'quantity'=>5,'description'=>'description','categorieid'=>2],
            ['name'=>'Armadura','price'=>100,'quantity'=>5,'description'=>'descripción así larga para ver si la tabla es capaz de soportar un texto tan largo y ver que tal se ve.','categorieid'=>3]
        ]);

        DB::table('customers')->insert([
            ['name'=>'David','address'=>"Casa 1"],
            ['name'=>'Chen','address'=>"Casa 2"],
            ['name'=>'Eric','address'=>"Casa 3"],
        ]);

    }
}
