<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('customers')->insert([
            ['name'=>'David','address'=>"Casa 1"],
            ['name'=>'Chen','address'=>"Casa 2"],
            ['name'=>'Eric','address'=>"Casa 3"],
        ]);
    }
}
