<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products',function(Blueprint $table){
            $table -> id()->primary();
            $table -> foreignId("categorieid")->references("id")->on("categories");
            $table -> string('name')->unique();
            $table -> integer('price');
            $table -> integer('quantity')->unsigned()->default(1);
            $table -> string('description');
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {

    }
};
