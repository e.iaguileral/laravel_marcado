<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('commands_products', function (Blueprint $table) {
            $table->id();
            $table->foreignId("commandid")->references("id")->on("commands")->onDelete('cascade');
            $table->foreignId("productid")->references("id")->on("products")->onDelete('cascade');
            $table->double("price");
            $table->integer("quantity")->unsigned();
            $table->integer("iva");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('commands_products');
    }
};
