<div class="cabezera">
    <h1 class="titulo">Projecte Laravel</h1>
    <div class="nav">
        <a class="enllaç" onclick="location.href='{{url('/products')}}'">Productes</a>
        <a class="enllaç" onclick="location.href='{{url('/categories')}}'">Categories</a>
        <a class="enllaç" onclick="location.href='{{url('/customers')}}'">Clients</a>
        <a class="enllaç" onclick="location.href='{{url('/seeCommands')}}'">Factures</a>
        <a class="enllaç" onclick="location.href='{{url('/formCommandCustomer')}}'">Comprar</a>
    </div>
</div>

<div class="contingut">
    <h1>Comprar producte:</h1>
    <form method="POST" action="{{route('addcommand')}}">
        @csrf
        <label>Client: </label><select name="customer">
            @foreach($customers as $customer)
                <option value="{{$customer->id}}">{{$customer->name}}</option>
            @endforeach
        </select>
        <br><br>
        <label>Producte: </label><select name="product">
            @foreach($products as $product)
                <option value="{{$product->id}}">{{$product->name}}</option>
            @endforeach
        </select>
        <label>Quantitat: </label><input type="number" id="quantity" name="quantity">
        <br><br>
        <button class="comprar" type="submit">Comprar</button>
        @if($fallo)
            <p>Ha d'haver-hi una quantitat i ha de ser menor a la quantitat total d'estoc del producte.</p>
        @endif
    </form>

</div>

<style>
    .cabezera{
        padding-top:5px;
        background-color: red;
    }
    .nav{
        display: grid;
        grid-template-columns: repeat(5, 1fr);
    }
    .titulo{
        color: white;
        margin-left: 20px;
    }
    .enllaç{
        background-color: darkred;
        color: white;
        text-align: center;
    }

    .contingut{
        padding-top: 2%;
        padding-left: 8%;
    }

    .comprar{
        background-color: red;
        color: white;
        border: none;
        border-radius: 20px;
        font-weight: bold;
        font-size: 20px;
        width: 20%;
        height: 10%;

    }
</style>
