<div>
<div class="cabezera">
    <h1 class="titulo">Projecte Laravel</h1>
    <div class="nav">
        <a class="enllaç" onclick="location.href='{{url('/products')}}'">Productes</a>
        <a class="enllaç" onclick="location.href='{{url('/categories')}}'">Categories</a>
        <a class="enllaç" onclick="location.href='{{url('/customers')}}'">Clients</a>
        <a class="enllaç" onclick="location.href='{{url('/seeCommands')}}'">Factures</a>
        <a class="enllaç" onclick="location.href='{{url('/formCommandCustomer')}}'">Comprar</a>
    </div>
</div>
<div class="contingut">
    <h1>Factures: </h1>
    <table>
        <thead>
        <tr>
            <th>Nom client</th>
            <th>Productes</th>
            <th>Preu sense IVA</th>
            <th>Preu amb IVA</th>
            <th>Data</th>
        </tr>
        </thead>
        <tbody>
        @foreach($commands as $command)
            <tr>
                <td>{{$command->customer->name}}</td>
                <td><ul>
                        @foreach ($command->products as $product)
                            <li>{{ $product->name }}</li>
                        @endforeach
                    </ul></td>
                <td>{{$command->total_price}}</td>
                <td>{{$command->total_price_IVA}}</td>
                <td>{{$command->data}}</td>

            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>
<style>
    .cabezera{
        padding-top:5px;
        background-color: red;
    }
    .nav{
        display: grid;
        grid-template-columns: repeat(5, 1fr);
    }
    .titulo{
        color: white;
        margin-left: 20px;
    }
    .enllaç{
        background-color: darkred;
        color: white;
        text-align: center;
    }
    .contingut{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    table {
        width: 50%;
        border-collapse: collapse;
    }

    th{
        border: 1px solid black;
        text-align: center;
    }

    td{
        border: 1px solid black;
        word-break: break-all;
    }

    th:nth-child(1),
    td:nth-child(1) {
        width: 10%;
    }

    th:nth-child(2),
    td:nth-child(2) {
        width: 10%;
    }

    th:nth-child(3),
    td:nth-child(3) {
        width: 10%;
        text-align: center;
    }

    th:nth-child(4),
    td:nth-child(4) {
        width: 10%;
        text-align: center;
    }
    th:nth-child(5),
    td:nth-child(5) {
        width: 7%;
        text-align: center;
    }
</style>
