<div>
    <div class="cabezera">
        <h1 class="titulo">Projecte Laravel</h1>
        <div class="nav">
            <a class="enllaç" onclick="location.href='{{url('/products')}}'">Productes</a>
            <a class="enllaç" onclick="location.href='{{url('/categories')}}'">Categories</a>
            <a class="enllaç" onclick="location.href='{{url('/customers')}}'">Clients</a>
            <a class="enllaç" onclick="location.href='{{url('/seeCommands')}}'">Factures</a>
            <a class="enllaç" onclick="location.href='{{url('/formCommandCustomer')}}'">Comprar</a>
        </div>
    </div>
    <div class="contingut">
        <h1>Productes: </h1>
        <table>
            <thead>
            <tr>
                <th>Nom</th>
                <th>Preu</th>
                <th>Quantitat</th>
                <th>Descripció</th>
                <th>Categoria</th>
                <th>Esborrar</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{$product->name}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->quantity}}</td>
                    <td>{{$product->description}}</td>
                    <td>{{$product->categories->name}}</td>
                    <td><button type="button" onclick="location.href='{{url('/products/'.$product->id.'/delete')}}'">Delete</button></td>
                </tr>
            @endforeach
            </tbody>


        </table>
        <button class="afegir" type="button" onclick="location.href='{{url('/formProducts')}}'">Afegir productes</button>
    </div>
</div>


<style>
    .cabezera{
        padding-top:5px;
        background-color: red;
    }
    .nav{
        display: grid;
        grid-template-columns: repeat(5, 1fr);
    }
    .titulo{
        color: white;
        margin-left: 20px;
    }
    .enllaç{
        background-color: darkred;
        color: white;
        text-align: center;
    }
    .afegir{
        margin-top: 20px;
        width: 15%;
    }
    .contingut{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    table {
        width: 70%;
        border-collapse: collapse;
    }

    th{
        border: 1px solid black;
        text-align: center;
    }

    td{
        border: 1px solid black;
        word-break: break-all;
    }

    th:nth-child(1),
    td:nth-child(1) {
        width: 15%;
    }

    th:nth-child(2),
    td:nth-child(2) {
        width: 5%;
        text-align: center;
    }

    th:nth-child(3),
    td:nth-child(3) {
        width: 5%;
        text-align: center;
    }

    th:nth-child(4),
    td:nth-child(4) {
        width: 30%;
    }
    th:nth-child(5),
    td:nth-child(5) {
        width: 15%;
    }
    th:nth-child(6),
    td:nth-child(6) {
        width: 5%;
        text-align: center;
    }
</style>
