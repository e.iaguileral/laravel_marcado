<div>
<div class="cabezera">
    <h1 class="titulo">Projecte Laravel</h1>
    <div class="nav">
        <a class="enllaç" onclick="location.href='{{url('/products')}}'">Productes</a>
        <a class="enllaç" onclick="location.href='{{url('/categories')}}'">Categories</a>
        <a class="enllaç" onclick="location.href='{{url('/customers')}}'">Clients</a>
        <a class="enllaç" onclick="location.href='{{url('/seeCommands')}}'">Factures</a>
        <a class="enllaç" onclick="location.href='{{url('/formCommandCustomer')}}'">Comprar</a>
    </div>
</div>
<div class="contingut">
    <h1>Categories: </h1>
    <table>
        <thead>
        <tr>
            <th>Nom</th>
            <th>IVA</th>
            <th>Esborrar</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $categorie)
            <tr>
                <td>{{$categorie->name}}</td>
                <td>{{$categorie->percent}}</td>
                <td><button type="button" onclick="location.href='{{url('/categories/'.$categorie->id.'/delete')}}'">Delete</button></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <button class="afegir" type="button" onclick="location.href='{{url('/formCategories')}}'">Afegir categories</button>
</div>
</div>
<style>
    .cabezera{
        padding-top:5px;
        background-color: red;
    }
    .nav{
        display: grid;
        grid-template-columns: repeat(5, 1fr);
    }
    .titulo{
        color: white;
        margin-left: 20px;
    }
    .enllaç{
        background-color: darkred;
        color: white;
        text-align: center;
    }
    .afegir{
        margin-top: 20px;
        width: 15%;
    }
    .contingut{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    table {
        width: 20%;
        border-collapse: collapse;
    }

    th{
        border: 1px solid black;
        text-align: center;
    }

    td{
        border: 1px solid black;
        word-wrap: break-word;
        word-break: break-all;
    }

    th:nth-child(2),
    td:nth-child(2) {
        width: 15%;
        text-align: center;
    }

    th:nth-child(3),
    td:nth-child(3) {
        width: 15%;
        text-align: center;
    }
</style>
