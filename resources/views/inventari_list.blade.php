<div class="cabezera">
    <h1 class="titulo">Projecte Laravel</h1>
    <div class="nav">
        <a class="enllaç" onclick="location.href='{{url('/products')}}'">Productes</a>
        <a class="enllaç" onclick="location.href='{{url('/categories')}}'">Categories</a>
        <a class="enllaç" onclick="location.href='{{url('/customers')}}'">Clients</a>
        <a class="enllaç" onclick="location.href='{{url('/seeCommands')}}'">Factures</a>
        <a class="enllaç" onclick="location.href='{{url('/formCommandCustomer')}}'">Comprar</a>
    </div>
</div>
<div class="contingut">
    <h1>Inventari de {{$customer->name}}: </h1>
    <table>
        <thead>
        <tr>
            <th>Objecte</th>
            <th>Quantitat</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($customer->products as $product)
            <tr>
                <td>{{$product->name}}</td>
                <td>{{$product->pivot->quantity}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<style>
    .cabezera{
        padding-top:5px;
        background-color: red;
    }
    .nav{
        display: grid;
        grid-template-columns: repeat(5, 1fr);
    }
    .titulo{
        color: white;
        margin-left: 20px;
    }
    .enllaç{
        background-color: darkred;
        color: white;
        text-align: center;
    }
    .contingut{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    table {
        width: 30%;
        border-collapse: collapse;
    }

    th{
        border: 1px solid black;
        text-align: center;
    }

    td{
        border: 1px solid black;
        word-break: break-all;
    }

    th:nth-child(1),
    td:nth-child(1) {
        width: 80%;
    }

    th:nth-child(2),
    td:nth-child(2) {
        text-align: center;
    }

</style>
