<div class="cabezera">
    <h1 class="titulo">Projecte Laravel</h1>
    <div class="nav">
        <a class="enllaç" onclick="location.href='{{url('/products')}}'">Productes</a>
        <a class="enllaç" onclick="location.href='{{url('/categories')}}'">Categories</a>
        <a class="enllaç" onclick="location.href='{{url('/customers')}}'">Clients</a>
        <a class="enllaç" onclick="location.href='{{url('/seeCommands')}}'">Factures</a>
        <a class="enllaç" onclick="location.href='{{url('/formCommandCustomer')}}'">Comprar</a>
    </div>
</div>
<div class="contingut">
    <h1>Afegir producte:</h1>
    <form method="POST" action="{{route('addproducts')}}">
        @csrf
        <label>Nom: </label><input placeholder="name" name="name"><br><br>
        <label>Preu: </label><input placeholder="price" name="price"><br><br>
        <label>Quantitat d'unitats: </label><input placeholder="quantity" name="quantity"><br><br>
        <label>Descripció:</label><br><textarea name="description"></textarea><br><br>
        <label>Categoria: </label> <select name="categorie">
            @foreach($categories as $categorie)
                <option value="{{$categorie->id}}">{{$categorie->name}}</option>
            @endforeach
        </select><br><br>
        <button type="submit">Afegir producte</button>
    </form>
</div>

<style>
    .cabezera{
        padding-top:5px;
        background-color: red;
    }
    .nav{
        display: grid;
        grid-template-columns: repeat(5, 1fr);
    }
    .titulo{
        color: white;
        margin-left: 20px;
    }
    .enllaç{
        background-color: darkred;
        color: white;
        text-align: center;
    }

    .contingut{
        padding-top: 2%;
        padding-left: 8%;
    }
</style>
