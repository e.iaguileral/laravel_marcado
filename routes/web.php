<?php

use App\Http\Controllers\Products;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\CustomerController;
use \App\Http\Controllers\CommandController;
use \App\Http\Controllers\CategorieController;

Route::get('/', function () {
    return redirect('/products');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/products/{product}/delete',[Products::class,'deleteProduct'])->name('Products.deleteProduct');

Route::get('/customers/{customer}/delete',[CustomerController::class,'deleteCustomer'])->name('CustomerController.deleteCustomer');

Route::get('/customers/{customer}/inventari',[\App\Http\Controllers\InventariController::class,'showInventori'])->name('InventariController.showInventory');

Route::get('/products',[Products::class,'index'])->name('Products.index');

Route::get('/customers',[CustomerController::class,'index'])->name('CustomerController.index');

Route::get('/formProducts',[Products::class,'showForm'])->name('formProducts');
Route::post('/addproduct',[Products::class,'addProduct'])->name('addproducts');

Route::get('/formCustomers',[CustomerController::class,'showForm'])->name('formCustomers');
Route::post('/addCustomer',[CustomerController::class,'addCustomer'])->name('addCustomer');

Route::get('/formCommandCustomer',[CommandController::class,'formCommand'])->name('CommandController.formCommand');
Route::post('/addCommand',[CommandController::class,'addCommand'])->name('addcommand');

Route::get('/seeCommands',[CommandController::class,'allCommand'])->name('CommandController.allCommand');

Route::get('/categories',[CategorieController::class,'showCategories'])->name('CategorieController.showCategories');
Route::get('/categories/{categorie}/delete',[CategorieController::class,'deleteCategorie'])->name('CategorieController.deleteCategorie');

Route::get('/formCategories',[CategorieController::class,'showForm'])->name('formCategories');
Route::post('/addCategorie',[CategorieController::class,'addCategorie'])->name('addcategorie');
require __DIR__.'/auth.php';
